fun Int.checkWhenGrade() = when {
    this in 71..100 -> println("Nilai Anda sangat baik!")
    this in 61..85 -> println("Nilai Anda baik!")
    this in 51..60 -> println("Nilai Anda cukup!")
    this in 41..50 -> println("Nilai Anda kurang!")
    this in 0..40 -> println("Nilai Anda tidak lulus!")
    else -> println("Nilai tidak valid!")
}

fun Int.checkIfKelulusan() = if (this >= 85) {
    println("Selamat! Anda lulus ujian.")
} else {
    println("Maaf, Anda belum lulus ujian. Tetap semangat!")
}

fun Hewan.checkHewan() = if (this is Kucing) {
    this.identifikasiJenis()
} else if (this is Anjing) {
    this.identifikasiJenis()
} else {
    println("Hewan tidak teridentifikasi!")
}