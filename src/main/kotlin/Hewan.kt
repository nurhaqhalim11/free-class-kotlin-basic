open class Hewan(val nama: String, val berat: Double) {
    open fun bersuara() {
        print("$nama memiliki berat $berat dan bersuara ")
    }

    open fun identifikasiJenis() {

    }
}

class Kucing(nama: String, berat: Double) : Hewan(nama, berat) {
    override fun bersuara() {
        super.bersuara()
        println("Meow!")
    }

    override fun identifikasiJenis() {
        super.identifikasiJenis()
        println("Ini adalah hewan yang berjenis Kucing")
    }
}

class Anjing(nama: String, berat: Double) : Hewan(nama, berat) {
    override fun bersuara() {
        super.bersuara()
        println("Guk! Guk!")
    }

    override fun identifikasiJenis() {
        println("Ini adalah hewan yang berjenis Anjing")
    }
}