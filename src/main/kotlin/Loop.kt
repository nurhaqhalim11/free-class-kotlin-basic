fun List<Kucing>.whileLoop() {
    val i = 0
    while (i <= size-1) {
        this[i].bersuara()
    }
}

fun List<Kucing>.forLoop() {
    for (i in indices) {
        this[i].bersuara()
    }
}
