fun main(args: Array<String>) {
    typeDataAndVariables()
}

fun typeDataAndVariables() {
    val beratKucing = 15.3 //String type
    val beratAnjing = 18 // Int type

    val kucing = Kucing(nama = "Garfield", berat = beratKucing)
    val anjing = Anjing(nama = "Lucky", berat = beratAnjing.toDouble())

    kucing.bersuara()
    anjing.bersuara()
}

fun controlFlow() {
    val kucing = Kucing("Garfield", 18.0)
    kucing.checkHewan()

    val nilai = 86
    nilai.checkIfKelulusan()
    nilai.checkWhenGrade()
}

fun loopStatement() {
    val kumpulanKucing: List<Kucing> = mutableListOf(
        Kucing("Tom", 20.9),
        Kucing("Figaro", 18.5),
        Kucing("Goose", 15.5)
    )

    kumpulanKucing.whileLoop()
    kumpulanKucing.forLoop()
}